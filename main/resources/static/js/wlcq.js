var wlcqs= [];
loadData();

function showData(arr){
    list_div.innerHTML = "";
    if(arr.length==0){
        list_div.innerHTML = "暂无数据";
        return;
    }
    var allhtml = "";
    allhtml += "<table cellpadding='0' cellspacing='0'" +
        " width='100%'><tr class='theader'>" +
        "<th>超期物料名称</th><th>超期物料SN号</th><th>超期时间</th><th>超期类型</th><th>创建时间</th><th>状态</th><th>操作</th></tr>";
    for(var i=arr.length-1;i>=0;i--){
        var a = arr[i];
        allhtml +=
            "<tr class='list'>" +
            "<td class='ntitle'>"+a.wl_name+"</td>" +
            "<td class='ntitle'>"+a.wl_code+"</td>" +
            "<td class='ntitle'>"+a.chaoqi_time+"</td>" +
            "<td class='ntitle'>"+a.wlcq_type+"</td>" +
            "<td class='time'>"+a.date+"</td>" +
            "<td class='ntitle'>"+a.wlcq_status+"</td>" +
            "<td class='ntitle'><a class='edit_a' href='#'>编辑</a>" +
            "<a class='del_a' onclick='del("+a.id+")' href='javascript:;'>删除</a></td>" +
            "</tr>";
    }
    allhtml += "</table>";
    list_div.innerHTML = allhtml;
}

function search(){
    loadData({wl_name:stitle.value,wl_code:stitle.value});
}

function loadData(data){
    list_div.innerHTML = "";
    loading.style.display = "block";
    ajax("/find_all_wlcq",data,function(data){
        wlcqs = data;
        showData(wlcqs);
        loading.style.display = "none";
    });
}

function save(){
    var myDate=new Date()
    ajax("/add_wlcq",{
        wl_name:wl_name.value,
        wl_code:wl_code.value,
        chaoqi_time:chaoqi_time.value,
        wlcq_type:wlcq_type.value,
        wlcq_status:wlcq_status.value
       },function(data){
        alert("新增成功！");
        loadData();
        add_div.style.display = "none";
        list_div.style.display = "block";
        save_btn.style.display = "none";
        add_btn.style.display = "block";
    })
}

function del(id){
    if(!confirm("您确定要删除这个数据吗？")){
        return;
    }
    ajax("/del_wlcq", {id:id},function(data){
        alert("删除成功！");
        loadData();
    });
}
function add(){
    add_div.style.display = "block";
    list_div.style.display = "none";
    save_btn.style.display = "block";
    add_btn.style.display = "none";
}