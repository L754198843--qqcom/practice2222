var bwcks = [];
loadData();

function showData(arr){
    list_div.innerHTML = "";
    if(arr.length==0){
        list_div.innerHTML = "暂无数据";
        return;
    }
    var allhtml = "";
    allhtml += "<table cellpadding='0' cellspacing='0'" +
        " width='100%'><tr class='theader'>" +
        "<th>出库行为</th><th>创建时间</th><th>状态</th><th>操作</th><th>修改</th></tr>";
    for(var i=arr.length-1;i>=0;i--){
        var a = arr[i];
        
        allhtml +=
            "<tr class='list'>" +
            "<td class='ntitle'>"+a.behavior+"</td>" +
            "<td class='ntitle'>"+a.time+"</td>" +
            "<td class='ntitle'>"+a.status+"</td>" +
            "<td class='ntitle'>"+a.operation+"</td>" +
            "<td class='ntitle'><a class='edit_a' href='#'>编辑</a>" +
            "<a class='del_a' onclick='del("+a.id+")' href='javascript:;'>删除</a></td>" +
            "</tr>";
    }
    allhtml += "</table>";
    list_div.innerHTML = allhtml;


}

function search(){
    loadData({behavior:stitle.value,status:stitle.value,operation:stitle.value});
}

function loadData(data){
    loading.style.display = "block";
    ajax("/find_all_bwck",data,function(bwcks){
         showData(bwcks);
         loading.style.display = "none";
    });
}

function save(){

    ajax("/add_bwck",{
        behavior:behavior.value,
        status:status1.value,
        operation:operation1.value
        
    },function(data){
        alert("新增成功！");
        loadData();
        add_div.style.display = "none";
        list_div.style.display = "block";
        save_btn.style.display = "none";
        add_btn.style.display = "block";
    })

}


function del(id){
    if(!confirm("您确定要删除这个数据吗？")){
        return;
    }
    ajax("/del_bwck", {id:id},function(data){
       alert("删除成功！");
       loadData();
    });
}

function add(){
    add_div.style.display = "block";
    list_div.style.display = "none";
    save_btn.style.display = "block";
    add_btn.style.display = "none";
}