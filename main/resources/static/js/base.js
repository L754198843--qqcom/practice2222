function ajax(url,data,callback,error){
    var req = new XMLHttpRequest();
    req.open("post",url);
    req.setRequestHeader("content-Type","application/json;charset=utf-8");
    if(!data) data = {}
    req.send(JSON.stringify(data));
    req.onreadystatechange = function(){
        if(req.readyState==4&&req.status==200){
            callback(JSON.parse(req.responseText));
        }else{
            if(error)
                error(req.responseText)
        }
    }
}