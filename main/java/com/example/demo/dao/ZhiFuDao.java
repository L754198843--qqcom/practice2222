package com.example.demo.dao;

import java.util.List;
import java.util.Map;

public interface ZhiFuDao {
    List<Map> find_all_zhifu(Map map);
    long count_all_zhifu(Map map);
    int add_zhifu(Map map);
    int del_zhifu(Map map);
    int upd_zhifu(Map map);
}