package com.example.demo.dao;

import java.util.List;
import java.util.Map;

public interface WuLiaoLeiXingDao {
    List<Map> find_all_wuliaoleixing(Map map);
    long count_all_wuliaoleixing(Map map);
    int add_wuliaoleixing(Map map);
    int del_wuliaoleixing(Map map);
    int upd_wuliaoleixing(Map map);
}
