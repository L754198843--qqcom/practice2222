package com.example.demo.dao;

import java.util.List;
import java.util.Map;

public interface WuLiaoKuDao {
    List<Map> find_all_wuliaoku(Map map);
    long count_all_wuliaoku(Map map);
    int add_wuliaoku(Map map);
    int del_wuliaoku(Map map);
    int upd_wuliaoku(Map map);
}