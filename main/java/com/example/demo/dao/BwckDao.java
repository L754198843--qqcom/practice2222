package com.example.demo.dao;

import java.util.List;
import java.util.Map;

public interface BwckDao {
    List<Map> find_all_bwck(Map map);
    long count_all_bwck(Map map);
    int add_bwck(Map map);
    int del_bwck(Map map);
    int upd_bwck(Map map);
}