package com.example.demo.controller;

import com.example.demo.dao.NoticeDao;
import com.example.demo.dao.WuLiaoKuDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin
public class WuLiaoKuController {
    @Autowired
    WuLiaoKuDao wuLiaoKuDao;

    @PostMapping("/del_wuliaoku")
    public int del_wuliaoku(@RequestBody Map map){
        return wuLiaoKuDao.del_wuliaoku(map);
    }

    @PostMapping("/add_wuliaoku")
    public int add_wuliaoku(@RequestBody Map map){
        return wuLiaoKuDao.add_wuliaoku(map);
    }

    @PostMapping("/find_all_wuliaoku")
    public List<Map> find_all_wuliaoku(@RequestBody Map map) throws InterruptedException {

        return wuLiaoKuDao.find_all_wuliaoku(map);
    }
}

