package com.example.demo.controller;

import com.example.demo.dao.NoticeDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;
import java.util.Map;

@RestController
public class NoticeController {
    @Autowired
    NoticeDao noticeDao;

    @PostMapping("/add_notice")
    public int add_notice(@RequestBody Map map){
        return noticeDao.add_notice(map);
    }

    @PostMapping("/find_all_notice")
    public List<Map> find_all_notice(@RequestBody Map map) throws InterruptedException {
        Thread.sleep(2000);
        return noticeDao.find_all_notice(map);
    }
}
